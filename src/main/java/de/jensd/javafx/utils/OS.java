/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.javafx.utils;

/**
 *
 * @author Jens Deters
 */
public class OS {

    private static final String OS_NAME = System.getProperty("os.name").toLowerCase();
    
    public static final String LOCAL_APP_DATA_PATH_WINDOWS = System.getProperty("user.home") + "/AppData/Local";
    public static final String LOCAL_APP_DATA_PATH_MAC = System.getProperty("user.home") + "/Library/Application Support/";
    public static final String LOCAL_APP_DATA_PATH_LINUX = System.getProperty("user.home");
    

    public static String getSystemLocalAppDataPath(){
        if(isWindows()){
            return LOCAL_APP_DATA_PATH_WINDOWS;
        }
        else if(isLinux()){
            return LOCAL_APP_DATA_PATH_LINUX;
        }
        else if(isMac()){
            return LOCAL_APP_DATA_PATH_MAC;
        }
        return System.getProperty("user.home");
    }
    
    public static boolean isWindows7() {
        return OS_NAME.contains("windows 7");
    }

    public static boolean isWindows8() {
        return OS_NAME.contains("windows 8");
    }

    public static boolean isWindows() {
        return OS_NAME.contains("windows");
    }

    public static boolean isMac() {
        return OS_NAME.contains("mac");
    }

    public static boolean isLinux() {
        return OS_NAME.contains("nux");
    }
    
}
