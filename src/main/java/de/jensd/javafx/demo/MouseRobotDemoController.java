/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.javafx.demo;

import de.jensd.javafx.utils.MouseRobot;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 * @author Jens Deters
 */
public class MouseRobotDemoController {

    @FXML
    private Label xValueLabel;

    @FXML
    private Label sceneXValueLabel;

    @FXML
    private Label screenXValueLabel;

    @FXML
    private Label yValueLabel;

    @FXML
    private Label sceneYValueLabel;

    @FXML
    private Label screenYValueLabel;
    
    @FXML
    private Button pickAndDragButton;

   @FXML
    public void initialize() {
        
        pickAndDragButton.setOnMouseDragged((MouseEvent event) -> {
            Point2D mousePosition = MouseRobot.getMousePosition();
            Point2D mouseScreenPosition = MouseRobot.getMouseOnScreenPosition(event);
            Point2D mouseScenePosition = MouseRobot.getMouseInScenePosition(event);
            
            xValueLabel.setText(""+mousePosition.getX());
            yValueLabel.setText(""+mousePosition.getY());
            sceneXValueLabel.setText(""+mouseScenePosition.getX());
            sceneYValueLabel.setText(""+mouseScenePosition.getY());
            screenXValueLabel.setText(""+mouseScreenPosition.getX());
            screenYValueLabel.setText(""+mouseScreenPosition.getY());
        });
        
        
        
    }

}
