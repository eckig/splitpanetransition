#SplitPaneDividerSlider
***de.jensd.javafx.utils.SplitPaneDividerSlider***

Usage:

Create a SplitPaneDividerSlider instance to control the state of the desired SplitPane then e.g. let the state be controlled by a ToggleButton:  

```
#!java

SplitPaneDividerSlider leftSplitPaneDividerSlider = new SplitPaneDividerSlider(centerSplitPane, 0, SplitPaneDividerSlider.Direction.LEFT);

leftToggleButton.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
            leftSplitPaneDividerSlider.setAimContentVisible(t1);
        });

```

[SplitPaneDivider Demo](http://youtu.be/RkgVffjJwXw)
[SplitPaneDivider in picmodo](http://youtu.be/NrgW3PMa3YA)


#TabPaneDetacher
***de.jensd.javafx.utils.TabPaneDetacher***

Utility to make all Tabs of a TabPane detachable. Basically it should be possible to enable the detach capability of any JavaFX TabPane by just calling:


```
#!java

public class TabPaneDetacherDemoController {

    @FXML
    private TabPane demoTabPane;
    
    @FXML
    public void initialize() {
        TabPaneDetacher.create().makeTabsDetachable(demoTabPane);
    }    
    
}
```

  
A new window is created containing the content of the tab, the window title is set to the tab name value. On closing that window the tab is restored at the original position:  

[TabPabeDetacher Demo](http://youtu.be/ow54mmBgL7w)